<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Pilih Destinasi</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('asset-pilihDestinasi/css/PilihDestinasi.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('asset-pilihDestinasi/css/header.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('asset-pilihDestinasi/css/footer.css') }}">

    <!-- MONSERRAT -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" />
    <!-- POPPINS -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600&display=swap" />
    <!-- VOLKHOV -->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Volkhov" />

    <!-- FONTAWESOME 6 -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css">
</head>

<body>
    <!-- --------------------------------HEADER--------------------------- -->
    <div class="header">
        <div class="header-logo">
            <img src="{{ asset('asset-pilihDestinasi/asset/logo-Travearth-color.png') }}">
        </div>
        <div class="header-nav-left">
            <ul>
                <li><a href="/" class="nav-name">Home</a></li>
                <li><a href="/posts" class="nav-name">Blog</a></li>
                <li><a href="/category" class="nav-name">Category</a></li>
                <li><a href="#" class="nav-name">About Us</a></li>
                @auth
                    @if (auth()->user()->role == 'admin')
                        <li>
                            <div class="dropdown">
                                <a href="#" class="dropdown-toggle" data-bs-toggle="dropdown">
                                    Halo, {{ auth()->user()->name }}
                                </a>
                                <div class="dropdown-menu">
                                    <a href="/dashboard" class="dropdown-item">Dashboard</a>
                                    <div class="dropdown-divider"></div>
                                    <form action="/logout" method="post">
                                        @csrf
                                        <button class="logout" type="submit" class="dropdown-item">Logout</button>
                                    </form>
                                </div>
                            </div>
                        </li>
                    @else
                        <li>
                            <div class="dropdown">
                                <a href="#" class="dropdown-toggle" data-bs-toggle="dropdown">
                                    Halo, {{ auth()->user()->name }}
                                </a>
                                <div class="dropdown-menu">
                                    <a href="#" class="dropdown-item">Profile Setting</a>
                                    <a href="#" class="dropdown-item">Add Your Post</a>
                                    <div class="dropdown-divider"></div>
                                    <form action="/logout" method="post">
                                        @csrf
                                        <button class="logout" type="submit" class="dropdown-item">Logout</button>
                                    </form>
                                </div>
                            </div>
                        </li>
                    @endif
                @else
                    <li><a href="/login" class="nav-name">Login</a></li>
                    <li><a href="/register" class="button">Sign Up</a></li>
                @endauth
            </ul>
        </div>
    </div>
    <!-- ------------------------------END HEADER------------------------------ -->

    <!-- ---------------------------MAIN---------------------------- -->

    <!-- ---------------------- SEARCH ------------------------- -->
    <div class="search">
        <!-- Actual search box -->
        <div class="form-group has-search">
            <span class="fa fa-search form-control-feedback"></span>
            <input type="text" class="form-control" placeholder="Find Your Destination">
        </div>
    </div>
    <!-- END-SEARCH -->

    <!-- Content -->
    <div class="container-content">
        {{-- <div class="content-left"></div> --}}
        <div class="content-right">
            <!-- Display-Kategori -->
            <div class="content-ew">
                @foreach ($destinations as $destination)
                    <div class="content-ew-col">
                        <img src="{{ $destination->thumbnail }}" />
                        <!-- IMAGE -->
                        <div class="content-ew-col-title">
                            <div class="content-ew-col-title-left">
                                <h3>{{ $destination->title }}</h3>
                                <p>{{ $destination->place }}</p>
                                <a href="/detail/{{ $destination->id }}">
                                    <button>See More!</button>
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <!-- END-DISPLAYKATEGORI -->

            <!-- Banner -->
            <div class="container-ban">
                <div class="banner-dp">
                    <div class="banner-dp-left">
                        <h3>Log in to enjoy more benefits.</h3>
                        <p>Book faster, get and collect the rewards or discounts.</p>
                        <button>Sign Up</button>
                    </div>
                    <div class="banner-dp-right">
                        <img src="{{ asset('asset-pilihDestinasi/Asset/banner-dp.png') }}">
                    </div>
                </div>
            </div>
            <!-- End-Banner -->

            <!-- end display kategori -->
        </div>
    </div>
    <!-- end-Content -->
    <!-- -------------------------END MAIN-------------------------- -->

    <!-- ------------------------------FOOTER------------------------------ -->
    <div class="footer">
        <div class="footer-con">
            <div class="footer-row">
                <div class="footer-col-1">
                    <img src="Asset/Travearth-logo.png" />
                    <p>Will take you to the place where you can rest your soul</p>
                </div>
                <div class="footer-col-2">
                    <h4>Navigation</h4>
                    <ul>
                        <li>Home</li>
                        <li>Discovery</li>
                        <li>Price</li>
                        <li>About</li>
                    </ul>
                </div>
                <div class="footer-col-3">
                    <h4>Company</h4>
                    <ul>
                        <li>Privacy</li>
                        <li>Help Center</li>
                        <li>Term Of Services</li>
                    </ul>
                </div>
                <div class="footer-col-4">
                    <h4>Learning</h4>
                    <ul>
                        <li>Resources</li>
                        <li>Blog</li>
                        <li>FAQ</li>
                    </ul>
                </div>
                <div class="footer-col-5">
                    <h4>Our Social Media</h4>
                    <ul>
                        <li>Facebook</li>
                        <li>Twitter</li>
                        <li>Instagram</li>
                        <li>Youtube</li>
                    </ul>
                </div>
            </div>
            <p class="copyright">Copyright Pegasus - 2021</p>
        </div>
    </div>
    <!-- -------------------------END FOOTER-------------------------- -->

    <script scr=”js/pilihdestinasi.js”></script>

    <!-- Include jQuery -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>


    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"
        integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"
        integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous">
    </script>
</body>

</html>
