<!DOCTYPE html>
<html lang="en">

@include('dashboard.partial.head')

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        @include('dashboard.partial.sidebar')

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                @include('dashboard.partial.nav')

                <div class="container-fluid">
                    <div class="container-fluid">

                        <!-- Page Heading -->
                        <h1 class="h3 mb-2 text-gray-800">Create Destination</h1>
                        <p class="mb-4">Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae
                            ratione ea quasi aperiam
                            voluptatem odit nostrum quam, dolor vitae atque.</p>

                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">Add Destination</h6>
                            </div>
                            <div class="card-body">
                                <form action="/admin-destination" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="mb-3">
                                        <label for="category" class="form-label">Category</label>
                                        <input type="text" class="@error('category') is-invalid @enderror form-control"
                                            id="category" name="category" value="{{ old('category') }}">

                                        @error('category')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>

                                    <div class="mb-3">
                                        <label for="title" class="form-label">Title</label>
                                        <input type="text" class="@error('title') is-invalid @enderror form-control"
                                            id="title" name="title" value="{{ old('title') }}">

                                        @error('title')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>

                                    <div class="mb-3">
                                        <label for="place" class="form-label">Address</label>
                                        <input type="text" class="@error('place') is-invalid @enderror form-control"
                                            id="place" name="place" value="{{ old('place') }}">

                                        @error('place')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>

                                    <div class="mb-3">
                                        <label for="description" class="form-label">Description</label>
                                        <input type="text"
                                            class="@error('description') is-invalid @enderror form-control"
                                            id="description" name="description" value="{{ old('description') }}">

                                        @error('description')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>

                                    <div class="mb-3">
                                        <label for="destination" class="form-label">Destination</label>
                                        <input type="text"
                                            class="@error('destination') is-invalid @enderror form-control"
                                            id="destination" name="destination" value="{{ old('destination') }}">

                                        @error('destination')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label for="image" class="form-label">Foto</label>
                                        <br />
                                        <input type="file" name="image" class="" placeholder="Foto">
                                    </div>

                                    <div class="form-group">
                                        <label for="thumbnail" class="form-label">Thumbnail</label>
                                        <br />
                                        <input type="file" name="thumbnail" class="" placeholder="Foto">
                                    </div>

                                    <button type="submit" class="btn btn-primary">Add Destination</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- End of Main Content -->

                <!-- Footer -->
                <footer class="sticky-footer bg-white">
                    <div class="container my-auto">
                        <div class="copyright text-center my-auto">
                            <span>Copyright &copy; Your Website 2021</span>
                        </div>
                    </div>
                </footer>
                <!-- End of Footer -->

            </div>
            <!-- End of Content Wrapper -->

        </div>
        <!-- End of Page Wrapper -->

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>

        <!-- Logout Modal-->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Select "Logout" below if you are ready to end your current session.
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <a class="btn btn-primary" href="login.html">Logout</a>
                    </div>
                </div>
            </div>
        </div>
        @include('dashboard.partial.js')

</body>

</html>
