 <header>
     <div class="container-header">
         <div class="logo">
             <img src="{{ asset('Asset/Travearth.png') }}" alt="Logo Travearth" />
         </div>

         <div class="nav-links">
             <ul>
                 <li><a href="/" class="nav-name">Home</a></li>
                 <li><a href="/posts" class="nav-name">Blog</a></li>
                 <li><a href="/category" class="nav-name">Category</a></li>
                 {{-- <li><a href="#" class="nav-name">Price</a></li> --}}
                 <li><a href="#" class="nav-name">About Us</a></li>
                 @auth
                     @if (auth()->user()->role == 'admin')
                         {{-- <li><a href="#" class="nav-name">Halo, {{ auth()->user()->name }}</a></li>
                        <li><a href="/dashboard" class="nav-name">Dashboard</a></li> --}}
                         <li>
                             <div class="dropdown">
                                 <a href="#" class="dropdown-toggle" data-bs-toggle="dropdown">
                                     Halo, {{ auth()->user()->name }}
                                 </a>
                                 <div class="dropdown-menu">
                                     <a href="/dashboard" class="dropdown-item">Dashboard</a>
                                     <div class="dropdown-divider"></div>
                                     <form action="/logout" method="post">
                                         @csrf
                                         <button class="logout" type="submit" class="dropdown-item">Logout</button>
                                     </form>
                                     {{-- <a href="/logout" type="submit" method="post" class="dropdown-item">Logout</a> --}}
                                 </div>
                             </div>
                         </li>
                         {{-- <li class="nav-item dropdown">
                             <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                 data-bs-toggle="dropdown" aria-expanded="false">
                                 Halo, {{ auth()->user()->name }}
                             </a>
                             <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                 <li>
                                     <a class="dropdown-item" href="/dashboard">Dashboard</a>
                                 </li>
                                 <li>
                                     <hr class="dropdown-divider">
                                 </li>
                                 <li>
                                     <form action="/logout" method="post">
                                         @csrf
                                         <button type="submit" class="dropdown-item">Logout</button>
                                     </form>
                                 </li>
                             </ul>
                         </li> --}}
                         {{-- <li>
                            <form action="/logout" method="POST">
                                @csrf
                                <button type="submit" class="button">
                                    Logout
                                </button>
                            </form>
                        </li> --}}
                     @else
                         {{-- <li><a href="#" class="nav-name">Halo, {{ auth()->user()->name }}</a></li>
                        <li>
                            <form action="/logout" method="POST">
                                @csrf
                                <button type="submit" class="button">
                                    Logout
                                </button>
                            </form>
                        </li> --}}
                         <li>
                             <div class="dropdown">
                                 <a href="#" class="dropdown-toggle" data-bs-toggle="dropdown">
                                     Halo, {{ auth()->user()->name }}
                                 </a>
                                 <div class="dropdown-menu">
                                     <a href="#" class="dropdown-item">Profile Setting</a>
                                     <a href="#" class="dropdown-item">Add Your Post</a>
                                     <div class="dropdown-divider"></div>
                                     <form action="/logout" method="post">
                                         @csrf
                                         <button class="logout" type="submit" class="dropdown-item">Logout</button>
                                     </form>
                                 </div>
                             </div>
                         </li>
                     @endif
                 @else
                     <li><a href="/login" class="nav-name">Login</a></li>
                     <li><a href="/register" class="button">Sign Up</a></li>
                 @endauth
             </ul>
         </div>
     </div>
 </header>
