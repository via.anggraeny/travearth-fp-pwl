
    <!-- Scripts bootstrap-->
    <script src="{{ asset('js/app.js') }}" defer></script>
   
    <!-- Styles bootstrap-->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- MONSERRAT -->
    <link
      href="https://fonts.googleapis.com/css?family=Montserrat"
      rel="stylesheet"
    />
    <!-- POPPINS -->
    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600&display=swap"
    />
    <!-- VOLKHOV -->
    <link
      rel="stylesheet"
      type="text/css"
      href="//fonts.googleapis.com/css?family=Volkhov"
    />

    <!-- FONTAWESOME 6 -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css">
