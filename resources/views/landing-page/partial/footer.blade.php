	<!-- ------------------------------FOOTER------------------------------ -->
    <div class="footer">
        <div class="footer-con">
          <div class="footer-row">
            <div class="footer-col-1">
              <img src="Asset/Travearth-logo.png" />
              <p>Will take you to the place where you can rest your soul</p>
            </div>
            <div class="footer-col-2">
              <h4>Navigation</h4>
              <ul>
                <li>Home</li>
                <li>Discovery</li>
                <li>Price</li>
                <li>About</li>
              </ul>
            </div>
            <div class="footer-col-3">
              <h4>Company</h4>
              <ul>
                <li>Privacy</li>
                <li>Help Center</li>
                <li>Term Of Services</li>
              </ul>
            </div>
            <div class="footer-col-4">
              <h4>Learning</h4>
              <ul>
                <li>Resources</li>
                <li>Blog</li>
                <li>FAQ</li>
              </ul>
            </div>
            <div class="footer-col-5">
              <h4>Our Social Media</h4>
              <ul>
                <li>Facebook</li>
                <li>Twitter</li>
                <li>Instagram</li>
                <li>Youtube</li>
              </ul>
            </div>
          </div>
          <p class="copyright">Copyright Pegasus - 2021</p>
        </div>
      </div>
      <!-- -------------------------END FOOTER-------------------------- -->