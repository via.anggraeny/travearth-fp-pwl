<!-- --------------------------------HEADER--------------------------- -->
<div class="header">
    <div class="header-logo">
        <img src="{{ asset('asset-detail/asset/logo-Travearth-color.png') }}">
    </div>
    <div class="header-nav-left">
        <ul>
            <li><a href="#" class="nav-name">Home</a></li>
            <li><a href="#" class="nav-name">Blog</a></li>
            <li><a href="#" class="nav-name">Category</a></li>
            <li><a href="#" class="nav-name">About Us</a></li>
            @auth
                @if (auth()->user()->role == 'admin')
                    {{-- <li><a href="#" class="nav-name">Halo, {{ auth()->user()->name }}</a></li>
                        <li><a href="/dashboard" class="nav-name">Dashboard</a></li> --}}
                    <li>
                        <div class="dropdown">
                            <a href="#" class="dropdown-toggle" data-bs-toggle="dropdown">
                                Halo, {{ auth()->user()->name }}
                            </a>
                            <div class="dropdown-menu">
                                <a href="/dashboard" class="dropdown-item">Dashboard</a>
                                <div class="dropdown-divider"></div>
                                <a href="/logout" class="dropdown-item">Logout</a>
                            </div>
                        </div>
                    </li>
                @else
                    {{-- <li><a href="#" class="nav-name">Halo, {{ auth()->user()->name }}</a></li>
                        <li>
                            <form action="/logout" method="POST">
                                @csrf
                                <button type="submit" class="button">
                                    Logout
                                </button>
                            </form>
                        </li> --}}
                    <li>
                        <div class="dropdown">
                            <a href="#" class="dropdown-toggle" data-bs-toggle="dropdown">
                                Halo, {{ auth()->user()->name }}
                            </a>
                            <div class="dropdown-menu">
                                <a href="#" class="dropdown-item">Profile Setting</a>
                                <a href="#" class="dropdown-item">Add Your Post</a>
                                <div class="dropdown-divider"></div>
                                <a href="/logout" class="dropdown-item">Logout</a>
                            </div>
                        </div>
                    </li>
                @endif
            @else
                <li><a href="/register"><button>Sign Up</button></a></li>
            @endauth
        </ul>
    </div>
</div>
<!-- ------------------------------END HEADER------------------------------ -->
