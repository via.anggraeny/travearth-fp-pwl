<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Detail Perjalanan</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('asset-detail/detail.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('asset-detail/header.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('asset-detail/footer.css') }}">
    <!-- MONSERRAT -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" />
    <!-- POPPINS -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600&display=swap" />
    <!-- VOLKHOV -->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Volkhov" />

    <!-- FONTAWESOME 6 -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css">

    <!-- BOOTSTRAP 5.1 -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
</head>

<body>
    <!-- --------------------------------HEADER--------------------------- -->
    {{-- <div class="header">
        <div class="header-logo">
            <img src="{{ 'asset-detail/asset/logo-Travearth-color.png' }}">
        </div>
        <div class="header-nav-left">
            <ul>
                <li><a href="/">Home</a></li>
                <li><a href="">Discovery</a></li>
                <li><a href="">Price</a></li>
                <li><a href="">About Us</a></li>
                @auth
                    <li><a href="">Halo {{ auth()->user()->name }}</a></li>
                    <form action="/logout" method="POST">
                        @csrf
                        <button type="submit" class="button">
                            Logout
                        </button>
                    </form>
                @else
                    <li><a href="/regiter"><button>Sign Up</button></a></li>
                @endauth

            </ul>
        </div>
    </div> --}}
    @include('landing-page.partial.header')
    <!-- ------------------------------END HEADER------------------------------ -->

    <!-- ---------------------------MAIN---------------------------- -->

    <div class="content-img">
        <div class="content-img-main">
            <img src="{{ asset($destination->image) }}">
        </div>
    </div>


    <!-- ------------------------------DESTINASI------------------------------ -->
    <div class="content-destinasi">
        <div class="cont-dest-left">
            <span class="kategori-perjalanan"> {{ $destination->category }} </span>
            <h2>{{ $destination->title }}</h2>
            <div class="cont-dest-star">
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="far fa-star"></i>

                <span>Multipel Destination</span>
            </div>
            <div class="cont-dest-lokasi">
                <i class="fas fa-map-marker-alt"></i>
                <span>{{ $destination->place }}</span>
            </div>
            <p>{{ $destination->description }}</p>
        </div>
        <div class="cont-dest-right">
            <button>See More</button>
        </div>
    </div>
    <!-- ------------------------------END DESTINASI------------------------------ -->

    <!-- ----------------Deskripsi Destination------------------- -->
    <div class="cont-desc-destinasi">
        <span>Destination</span>
        <ul>
            <li>{{ $destination->destination }}</li>
        </ul>
    </div>

    <!-- ----------------- END Deskripsi Destination ------------------ -->

    <!-- ---------------------------Facility---------------------------- -->
    <div class="facilities-section">
        <h1>Facilities</h1>
        <div class="row-facility">
            <div class="facility">
                <i class="fas fa-wifi"></i>
                <h2>Wifi</h2>
                <p></p>
            </div>
            <div class="facility">
                <i class="fas fa-fan"></i>
                <h2>AC</h2>
                <p></p>
            </div>
            <div class="facility">
                <i class="fas fa-utensils"></i>
                <h2>Restaurant</h2>
                <p></p>
            </div>
            <div class="facility">
                <i class="fas fa-parking"></i>
                <h2>Parking</h2>
                <p></p>
            </div>
            <div class="facility">
                <i class="fas fa-swimmer"></i>
                <h2>Pool</h2>
                <p></p>
            </div>
            <div class="facility">
                <i class="fas fa-hot-tub"></i>
                <h2>SPA</h2>
                <p></p>
            </div>
            <div class="facility">
                <i class="fas fa-arrow-circle-up"></i>
                <h2>Lift</h2>
                <p></p>
            </div>
            <div class="facility">
                <i class="fas fa-child"></i>
                <h2>Kids Zone</h2>
                <p></p>
            </div>
        </div>
    </div>
    <!-- ---------------------------End Facility---------------------------- -->

    <!-- ---------------------------Dropdown---------------------------- -->
    <!-- ---------------------------End Dropdown---------------------------- -->

    <!-- ---------------------------Review---------------------------- -->
    <div class="review">
        <div class="review-container">
            <h1 class="title-review">Review</h1>
            <div class="review-content">
                <div class="review-content-col">
                    <div class="review-content-left">
                        <h3>Traveler</h3>
                        <h1>4.8/5</h1>
                        <p>From 476 review</p>
                    </div>
                    <div class="review-content-mid">
                        <div class="review-content-mid-container">
                            <div class="user-profile">
                                <img src="{{ asset('asset-detail/asset/avatar3.png') }}" alt="">
                            </div>
                            <div class="user-identity">
                                <h4>Fakhri Naufal F.</h4>
                                <p>11 October 2021 - Solo</p>
                            </div>
                            <div class="user-rating">
                                <div class="user-rating-container">
                                    <p>4.9/5</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="review-content-right">
                        <div class="comment-title">
                            <h2>It's was a great experience</h2>
                        </div>
                        <div class="rating-comment">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                        <div class="comment-user">
                            <p>
                                All hotel staff are friendly. The service is very good.
                                The rooms have been renovated and are really nice,
                                the room facilities are complete. Lighting in the room
                                makes you don't want to leave the room. The main course
                                breakfast is really delicious!!
                            </p>
                            <p>
                                There are free activities that can be done at the hotel
                                such as yoga, personal training, archery. Incidentally,
                                I took part in archery, the equipment was complete and
                                the purchaser was very friendly, respectful and patient.
                                Definitely come back!!
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ---------------------------End Review---------------------------- -->

    <!-- --------------------------Koment Testimonial---------------------------- -->

    <!-- --------------------------ENDKoment Testimonial---------------------------- -->
    <!-- -------------------------END MAIN-------------------------- -->

    <!-- ------------------------------FOOTER------------------------------ -->
    <div class="footer">
        <div class="footer-con">
            <div class="footer-row">
                <div class="footer-col-1">
                    <img src="{{ asset('asset-detail/Asset/Travearth-logo.png') }}" />
                    <p>Will take you to the place where you can rest your soul</p>
                </div>
                <div class="footer-col-2">
                    <h4>Navigation</h4>
                    <ul>
                        <li>Home</li>
                        <li>Discovery</li>
                        <li>Price</li>
                        <li>About</li>
                    </ul>
                </div>
                <div class="footer-col-3">
                    <h4>Company</h4>
                    <ul>
                        <li>Privacy</li>
                        <li>Help Center</li>
                        <li>Term Of Services</li>
                    </ul>
                </div>
                <div class="footer-col-4">
                    <h4>Learning</h4>
                    <ul>
                        <li>Resources</li>
                        <li>Blog</li>
                        <li>FAQ</li>
                    </ul>
                </div>
                <div class="footer-col-5">
                    <h4>Our Social Media</h4>
                    <ul>
                        <li>Facebook</li>
                        <li>Twitter</li>
                        <li>Instagram</li>
                        <li>Youtube</li>
                    </ul>
                </div>
            </div>
            <p class="copyright">Copyright Pegasus - 2021</p>
        </div>
    </div>
    <!-- -------------------------END FOOTER-------------------------- -->
</body>

</html>
