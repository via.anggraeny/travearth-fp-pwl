<!DOCTYPE html>
<html>
@include('landing-page.partial.head')

<body>
    <!-- HEADER LANDING PAGE -->
    <div class="landing-page">
        @include('landing-page.partial.head-home')

        <div class="body-area">
            <div class="container-body">
                <div class="left-part">
                    <div class="title">
                        <h1>Travel, enjoy</h1>
                        <h1>and live a new</h1>
                        <h1>and full life</h1>
                    </div>
                    <p class="text">
                        To get the best your adventure you just need to leave and go where
                        you like. We are waiting for you.
                    </p>
                </div>

                <div class="right-part">
                    <img src="{{ asset('Asset/Discovery.png') }}" alt="vector" class="vector" />
                </div>
            </div>
        </div>

        <div class="bottom-area">
            <div class="container-bottom"></div>
        </div>
    </div>
    <!-- END-HEADER -->

    <!-- CONTENT -->
    <div class="Content">
        <!-- EXSPLORE-WONDERFUL -->
        <div class="CONTAINER">
            <div class="container-ew">
                <div class="cont-ew-title">
                    <h2>Explore Wonderful Experience</h2>
                    <div class="cont-ew-navlink">
                        <ul>
                            <li>Popular</li>
                            <li>Most View</li>
                            <li>Recent</li>
                        </ul>
                    </div>
                </div>
                <div class="cont-ew-show">
                    <a href="/pilih-destinasi">Show More...</a>
                </div>
            </div>
            <!-- KONTEN-EXSPLORENYA -->
            <div class="content-ew">
                @foreach ($destinations as $destination)
                    <div class="content-ew-col">
                        <img src="{{ $destination->thumbnail }}" />
                        <!-- IMAGE -->
                        <div class="content-ew-col-title">
                            <div class="content-ew-col-title-left">
                                <h3>{{ $destination->title }}</h3>
                                <div class="content-ew-col-title-left-loc">
                                    <i class="fas fa-map-marker-alt"></i>
                                    <p>{{ $destination->place }}a</p>
                                </div>
                                <a href="/detail/{{ $destination->id }}">
                                    <button>See More!</button>
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <!-- END EXSPLORE-WONDERFUL -->

        <!-- BANNER -->
        <div class="CONTAINER">
            <div class="container-banner">
                <div class="content-banner">
                    <div class="cont-banner-left">
                        <img src="{{ asset('Asset/Luggage.png') }}" />
                    </div>
                    <div class="cont-banner-right">
                        <h1>It’s a big world out there, let’s explore with us!</h1>
                        <p>
                            Dont wait until tomorrow, discovery your adventure now and feel
                            the sensation of closeness to nature around you
                        </p>
                        <div class="content-banner-right-btn">
                            <button>Plan a Trip!</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END BANNER -->

        <!-- TOP DESTINASI -->
        <div class="CONTAINER">
            <div class="container-topdestinasi">
                <div class="topdestinasi">
                    <h1>Top Destinations</h1>
                    <div class="td-navlink">
                        <ul>
                            <li>Popular</li>
                            <li>New</li>
                            <li>Recent</li>
                        </ul>
                    </div>
                    <div class="content-td">
                        <div class="content-td-col">
                            <img src="{{ asset('Asset/bali-td.png') }}" />
                            <!-- Ukuran gambar 3000x4000px-->
                            <div class="content-td-col-loc">
                                <img src="{{ asset('Asset/Icon/location.png') }}" />
                                <p>Bali</p>
                            </div>
                        </div>
                        <div class="content-td-col">
                            <img src="{{ asset('Asset/bajo-td.png') }}" />
                            <!-- Ukuran gambar 3000x4000px-->
                            <div class="content-td-col-loc">
                                <img src="{{ asset('Asset/Icon/location.png') }}" />
                                <p>Bajo</p>
                            </div>
                        </div>
                        <div class="content-td-col">
                            <img src="{{ asset('Asset/jogja-td.png') }}" />
                            <!-- Ukuran gambar 3000x4000px-->
                            <div class="content-td-col-loc">
                                <img src="{{ asset('Asset/Icon/location.png') }}" />
                                <p>Jogja</p>
                            </div>
                        </div>
                        <div class="content-td-col">
                            <img src="{{ asset('Asset/toba-td.png') }}" />
                            <!-- Ukuran gambar 3000x4000px-->
                            <div class="content-td-col-loc">
                                <img src="{{ asset('Asset/Icon/location.png') }}" />
                                <p>Toba</p>
                            </div>
                        </div>
                    </div>

                    <a href="">Show More...</a>
                </div>
            </div>
        </div>

        <!-- END TOP DESTINASI -->

        <!-- CONTENT BLOG -->
        <div class="blog">
            <div class="container-blog">
                <div class="blog-title">
                    <h2>Our blog from adventurers</h2>
                </div>
                <div class="blog-content">
                    <div class="blog-content-col">
                        <div class="blog-content-left">
                            <img src="Asset/Rectangle 35.png" alt="Labuan Bajo" />
                        </div>

                        <div class="blog-content-right">
                            <div class="container-content-blog">
                                <div class="title-blog">
                                    <h1>An exploration of the Wondrous</h1>
                                    <h1>Labuan Bajo</h1>
                                </div>
                                <div class="detail">
                                    <p>
                                        Filled with wonders waiting to be discovered, Labuan bajo
                                        is a piece of heaven that a wait your arrival.
                                    </p>
                                    <div class="read-more">
                                        <p><a href="">Read more...</a></p>
                                    </div>
                                </div>
                                <div class="author author1">
                                    <div class="name">
                                        <h6>Peter Parker</h6>
                                    </div>
                                    <div class="dot"></div>
                                    <div class="date">
                                        <h6>7 May 2021</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="blog-content-col">
                        <div class="blog-content-left">
                            <img src="Asset/Rectangle 44.png" alt="Borobudur" />
                        </div>

                        <div class="blog-content-right">
                            <div class="container-content-blog">
                                <div class="title-blog">
                                    <h1>Borobudur - The Grand Temple</h1>
                                </div>
                                <div class="detail">
                                    <p>
                                        Located on the island of Java, the magnificent Borobudur
                                        temple is the world's biggest Buddhist monument. The
                                        temple sits majestically on a hilltop overlooking lush
                                        green fields and distant hills.
                                    </p>
                                    <div class="read-more">
                                        <p><a href="">Read more...</a></p>
                                    </div>
                                </div>
                                <div class="author author2">
                                    <div class="name">
                                        <h6>Mary Jane</h6>
                                    </div>
                                    <div class="dot"></div>
                                    <div class="date">
                                        <h6>23 June 2021</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="blog-content-col">
                        <div class="blog-content-left">
                            <img src="Asset/Rectangle 53.png" alt="Labuan Bajo" />
                        </div>

                        <div class="blog-content-right">
                            <div class="container-content-blog">
                                <div class="title-blog">
                                    <h1>Mandalika - Where The Waves Collide</h1>
                                </div>
                                <div class="detail">
                                    <p>
                                        Explore the tropical haven specifically designed to
                                        maximize your holiday experience. Locations : Mandalika,
                                        Central Lombok Going to Mandalika
                                    </p>
                                    <div class="read-more">
                                        <p><a href="">Read more...</a></p>
                                    </div>
                                </div>
                                <div class="author author3">
                                    <div class="name">
                                        <h6>Neds</h6>
                                    </div>
                                    <div class="dot"></div>
                                    <div class="date">
                                        <h6>2 August 2021</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END BLOG -->

        <!-- TESTIMONIAL -->
        <div class="testimonial">
            <h2>What They Said</h2>

            <div class="testimonial-cards">
                <div class="card">
                    <img src="Asset/avatar3.png" alt="Emila Tan" />
                    <p>
                        I had a fantastic experience with both my Bali and Lombok trip. In
                        Bali I was in Kuta Beach and GWK. The hotel in Bali is one of the
                        best I have ever stayed and everything we ate was delicious. It
                        was memorable and fun trip.
                    </p>
                    <div class="name-role">
                        <h3>Emila Tan</h3>
                        <h5>IT Consultant</h5>
                    </div>
                </div>

                <div class="card">
                    <img src="Asset/avatar2.png" alt="Tamara Ramsey" />
                    <p>
                        I would definitely recommend for all your travel planning needs
                    </p>
                    <div class="name-role">
                        <h3>Tamara Ramsey</h3>
                        <h5>Fullstack Developer</h5>
                    </div>
                </div>

                <div class="card">
                    <img src="Asset/avatar1.png" alt="Chris Wong" />
                    <p>
                        My trip to Bromo and Borobudur was very well-planned and
                        organized, and all minute details were taken care off. Travearth
                        has sincere, diligent, and dedicated resources who assisted us in
                        exceptionally efficient and quick manner.
                    </p>
                    <div class="name-role">
                        <h3>Chris Wong</h3>
                        <h5>Bussiness Analyst</h5>
                    </div>
                </div>
            </div>
        </div>
        <!-- END TESTIMONIAL -->

        <!-- CONTENT SUBSCRIBE -->
        <div class="subscribe">
            <div class="content-subs">
                <label>Subscribe to get information, latest news and other interesting
                    offers about Travearth</label>
                <div class="content-subs-input">
                    <input type="text" name="" placeholder="your email" />
                    <button>Subscribe</button>
                </div>
            </div>
        </div>
        <!-- END-SUBSCRIBE	 -->
    </div>
    <!-- END-CONTENT -->

    @include('landing-page.partial.footer')
</body>

</html>
