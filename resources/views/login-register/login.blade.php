<html lang="en">

<head>
    <meta charset="UTF-8" />
    <title>Login Page | Travearth</title>

    @include('landing-page.partial.library')

    <link rel="stylesheet" href="{{ asset('css/login.css') }}" />
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;1,100;1,200&display=swap" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css" />


</head>

<body>
    <div class="row">
        <div class="left-side">
            <div class="content">
                <img src="{{ asset('Asset_Login_Register/side-content.png') }}">
            </div>
        </div>

        <div class="right-side">
            <div class="logo">
                <img src="{{ asset('Asset_Login_Register/Travearth.png') }}">
            </div>
            <form class="container" action="/login" method="post">
                @csrf
                <h1>Login to your account</h1>
                <div class="email">
                    <h5>Email</h5>
                </div>
                <input type="email" name="email" id="email" placeholder="" class="holder">
                <div class="password">
                    <h5>Password</h5>
                </div>
                <input type="password" name="password" id="password" placeholder="" class="holder">
                <!---<input type="submit" value="Login now">--->
                <button class="btn">Login now</button>
                <div class="label">
                    <label>Don’t have an account? </label>
                    <a href="/register"> Sign Up</a>
                </div>
            </form>
        </div>
    </div>

    {{-- <div class="row">
      <div class="left-side">
        <div class="content">
          <img src="{{ asset('Asset_Login_Register/side-content.jpg') }}" />
        </div>
      </div>

      <div class="right-side">
        <div class="logo">
          <img src="{{ asset('Asset_Login_Register/Travearth.png') }}" />
        </div>
        <form class="container" action="/login" method="post">
            @csrf
          <h2>Login to your account</h2>
          <div class="email">
            <h3>Email</h3>
          </div>
          <input type="email" name="email" id="email" placeholder="" class="holder" />
          <div class="password">
            <h3>Password</h3>
          </div>
          <input type="password" name="password" id="password" placeholder="" class="holder" />
          
          <!---<input type="submit" value="Login now">--->
          <button class="btn">Login now</button>
          <hr/>
          <div class="label">
            <label>Don’t have an account?</label>
            <a href="/register"> Sign Up?</a>
          </div>
        </form>
      </div>
    </div> --}}
</body>

</html>
