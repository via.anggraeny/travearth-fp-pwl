<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Registraion | Travearth</title>

    @include('landing-page.partial.library')

    <link rel="stylesheet" href="{{ asset('css/registration 1.2.css') }}">
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;1,100;1,200&display=swap">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css">
</head>

<body>
    <div class="row">
        <div class="left-side">
            <div class="logo">
                <img src="{{ asset('Asset_Login_Register/Travearth.png') }}">
            </div>
            <div class="container">
                <div class="cont-left">
                    <h1>Complete Your Profile!</h1>
                    <form action="/register" method="post">
                        @csrf
                        <div class="input">
                            <div class="input1">
                                <div class="input11">
                                    <h3>Name</h3>
                                    <input type="text" name="name" id="name" placeholder="" class="holder">
                                </div>

                                <div class="input12">
                                    <h3>Phone Number</h3>
                                    <input type="text" name="phone_number" id="phone_number" placeholder=""
                                        class="holder">
                                </div>

                                <div class="input13">
                                    <h3>Email</h3>
                                    <input type="email" name="email" id="email" placeholder="" class="holder">
                                </div>
                            </div>
                            <div class="input2">
                                <div class="input21">
                                    <h3>NIK</h3>
                                    <input type="text" name="nik" id="nik" placeholder="" class="holder">
                                </div>

                                <div class="input22">
                                    <h3>Password</h3>
                                    <input type="password" name="password" id="password" placeholder=""
                                        class="holder">
                                </div>

                                <div class="input23">
                                    <h3>Gender</h3>
                                    <input type="text" name="gender" id="gender" placeholder="" class="holder">
                                </div>
                            </div>
                        </div>
                        <button class="btn">Registration Now</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="right-side">
            <div class="content">
                <img src="{{ asset('Asset_Login_Register/Rectangle 5.png') }}">
            </div>
        </div>
    </div>
</body>

</html>
