<?php

namespace App\Http\Controllers;

use App\Models\Destination;
use Illuminate\Http\Request;

class PilihDestinasiController extends Controller
{
    public function index()
    {
        return view('pilih-destinasi.pilih-destinasi', [
            'destinations' => Destination::latest()->take(20)->get()
        ]);
    }
}
