<?php

namespace App\Http\Controllers;

use App\Models\Destination;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        return view('landing-page.index', [
            'destinations' => Destination::latest()->take(4)->get()
        ]);
    }

    public function detail($id)
    {
        $data = Destination::findOrFail($id);
        return view('landing-page.detail', [
            'destination' => $data
        ]);
    }
}
