<?php

namespace App\Http\Controllers;

use App\Models\Destination;
use Illuminate\Http\Request;

class DestinationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.destination.destination', [
            'destinations' => Destination::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.destination.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'category' => 'required|max:255',
            'title' => 'max:255',
            'place' => 'required|max:255',
            'description' => 'required|max:255',
            'destination' => 'required',
        ]);

        $img_path = "";
        if ($request->hasFile('image')) {
            $image = $request->image;
            $image_name = time() . $image->getClientOriginalName();
            $image->move('upload/image/', $image_name);

            $img_path = 'upload/image/' . $image_name;
        }

        $thumbnail_path = "";
        if ($request->hasFile('thumbnail')) {
            $thumbnail = $request->thumbnail;
            $thumbnail_name = time() . $thumbnail->getClientOriginalName();
            $thumbnail->move('upload/thumbnail/', $thumbnail_name);

            $thumbnail_path = 'upload/thumbnail/' . $thumbnail_name;
        }

        $validatedData['image'] = $img_path;
        $validatedData['thumbnail'] = $thumbnail_path;

        Destination::create($validatedData);
        return redirect('/admin-destination');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Destination  $destination
     * @return \Illuminate\Http\Response
     */
    public function show(Destination $destination)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Destination  $destination
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $destination = Destination::findOrFail($id);
        return view('dashboard.destination.edit', [
            'destination' => $destination
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Destination  $destination
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'category' => 'required|max:255',
            'title' => 'max:255',
            'place' => 'required|max:255',
            'description' => 'required|max:255',
            'destination' => 'required',
        ]);

        $Destination = Destination::findOrFail($id);
        $Destination->category = $request->get('category');
        $Destination->title = $request->get('title');
        $Destination->place = $request->get('place');
        $Destination->description = $request->get('description');
        $Destination->destination = $request->get('destination');

        $img_path = $Destination->image;
        if ($request->hasFile('image')) {
            $image = $request->image;
            $image_name = time() . $image->getClientOriginalName();
            $image->move('upload/image/', $image_name);

            $img_path = 'upload/image/' . $image_name;
        }

        $thumbnail_path = $Destination->thumbnail;
        if ($request->hasFile('thumbnail')) {
            $thumbnail = $request->thumbnail;
            $thumbnail_name = time() . $thumbnail->getClientOriginalName();
            $thumbnail->move('upload/thumbnail/', $thumbnail_name);

            $thumbnail_path = 'upload/thumbnail/' . $thumbnail_name;
        }

        $Destination->image = $img_path;
        $Destination->thumbnail = $thumbnail_path;

        $Destination->save();
        return redirect('/admin-destination');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Destination  $destination
     * @return \Illuminate\Http\Response
     */
    public function destroy(Destination $destination, $id)
    {
        $destination = Destination::destroy($id);
        return redirect('/admin-destination');
    }
}
