<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        return view('dashboard.user.user', [
            'users' => User::all()
        ]);
    }

    public function show(User $user)
    {
        //
    }
}
