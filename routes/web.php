<?php

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\DestinationController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\PilihDestinasiController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [DashboardController::class, 'index']);
Route::get('/detail/{id}', [DashboardController::class, 'detail']);

Route::get('/dashboard', function () {
    return view('dashboard.dashboard');
})->middleware('auth');

Route::resource('/admin-destination', DestinationController::class);
Route::resource('/users', UserController::class);

Route::get('/register', [RegisterController::class, 'index']);
Route::post('/register', [RegisterController::class, 'store']);

Route::get('/login', [LoginController::class, 'index'])->name('login')->middleware('guest');
Route::post('/login', [LoginController::class, 'authenticate']);
Route::post('/logout', [LoginController::class, 'logout']);

Route::get('/posts', [PostController::class, 'index']);
Route::get('/pilih-destinasi', [PilihDestinasiController::class, 'index']);
